CREATE OR REPLACE TRIGGER hlidej_zanr
    BEFORE INSERT
    ON FILM_ZANR
    REFERENCING NEW AS new
    FOR EACH ROW
DECLARE
    zanr_count NUMBER;
BEGIN
    SELECT COUNT(ZANR_KOD) into zanr_count FROM FILM_ZANR WHERE ID_FILMU = :new.ID_FILMU;

    IF zanr_count > 3
    THEN
        RAISE_APPLICATION_ERROR(-20002, 'Film ' || :new.ID_FILMU || ' nemůže být přiřazen do více než 4 žánrů.');
    end if;
end;