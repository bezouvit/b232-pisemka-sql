CREATE OR REPLACE FUNCTION kolik_penez(id_zamestnance IN NUMBER)
    RETURN number IS
    total number;
BEGIN
    SELECT COUNT(ID_POZICE) INTO total FROM ZAM_HISTORIE WHERE ID_ZAM = id_zamestnance;

    IF total = 0
    THEN
        RAISE_APPLICATION_ERROR(-20001, 'Zaměstnanec s číslem ' || id_zamestnance ||
                                       ' buď neexistuje nebo nemá žádnou historii.');
    end if;

    FOR entry IN (SELECT * FROM ZAM_HISTORIE WHERE ID_ZAM = id_zamestnance)
        LOOP
            IF entry.DO is null
            then
                total := total + (abs(round(months_between(entry.OD, CURRENT_DATE))) * entry.PLAT);
            else
                total := total + (abs(round(months_between(entry.OD, entry.DO))) * entry.PLAT);
            end if;
        end loop;

    RETURN total;
end;